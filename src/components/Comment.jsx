import React from "react";
import styled from "styled-components";

const Container = styled.div`
  display: flex;
  gap: 10px;
  margin: 30px 0px;
`;

const Avatar = styled.img`
  width: 50px;
  height: 50px;
  border-radius: 50%;
`;

const Details = styled.div`
  display: flex;
  flex-direction: column;
  gap: 10px;
  color: ${({ theme }) => theme.text}
`;
const Name = styled.span`
  font-size: 13px;
  font-weight: 500;
`;

const Date = styled.span`
  font-size: 12px;
  font-weight: 400;
  color: ${({ theme }) => theme.textSoft};
  margin-left: 5px;
`;

const Text = styled.span`
  font-size: 14px;
`;

const Comment = () => {
  return (
    <Container>
      <Avatar src="https://www.itl.cat/pngfile/big/106-1062832_35-beginners-tips-best-eye-makeup-tutorials-girl.jpg" />
      <Details>
        <Name>
          Sevitha <Date>1 day ago</Date>
        </Name>
        <Text>
        what a great movie...did not expect that level of acting and amazing direction. Felt engaged from start to end.
        </Text>
      </Details>
    </Container>
  );
};

export default Comment;